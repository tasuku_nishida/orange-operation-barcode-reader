import Foundation

protocol BarcodeReaderProtocol {
  
  func setup()
  func open()
  
  var RNOrangeOperationBarcodeReader: RNOrangeOperationBarcodeReader { get }
}

