import Foundation

enum BarcodeReaderSDK: Int {
  case OPN3200Si
}

enum BarcodeReaderStd: Int {
  case IP
  case BLUETOOTH
}

class BarcodeReaderProperty {
  let BARCODE_READER_SDK = "BARCODE_READER_SDK"
  let BARCODE_READER_STD = "BARCODE_READER_STD"
  let BARCODE_READER_IP = "BARCODE_READER_IP"
  let BARCODE_READER_TIMEOUT_MILI = "BARCODE_READER_TIMEOUT_MILI"
  
  var barcodeReaderSdk : BarcodeReaderSDK
  var barcodeReaderStd : BarcodeReaderStd
  var barcodeReaderIP: String = ""
  var barcodeReaderTimeoutMili: Int = 10000
  
  var bridge: RNOrangeOperationBarcodeReader
  
  init(dict: [String : Any]) {
    barcodeReaderSdk = BarcodeReaderSDK(rawValue: (dict[BARCODE_READER_SDK] as? Int)!)!
    barcodeReaderStd = BarcodeReaderStd(rawValue: (dict[BARCODE_READER_STD] as? Int)!)!
    bridge = dict["bridge"] as! RNOrangeOperationBarcodeReader
    barcodeReaderIP = dict[BARCODE_READER_IP] as! String
    
    let timeouMili = dict[BARCODE_READER_TIMEOUT_MILI] as? Int
    if timeouMili != nil {
      barcodeReaderTimeoutMili = timeouMili!
    }
  }
}
