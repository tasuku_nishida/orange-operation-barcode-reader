import Foundation


@objc(RNOrangeOperationBarcodeReader)
class RNOrangeOperationBarcodeReader: RCTEventEmitter {
  
  var barcodeReaderProtocol: BarcodeReaderProtocol?
    
  override static func requiresMainQueueSetup() -> Bool {
    return true;
  }

  override func supportedEvents() -> [String]! {
    return ["barcode"]
  }
  
  @objc(setBarcodeReaderProperty:)
  func setBarcodeReaderProperty(barcodeReaderDic: NSDictionary) {
    var _barcodeReaderPropertyDic = barcodeReaderDic as! [String : Any]
    _barcodeReaderPropertyDic["bridge"] = self
    let barcodeReaderProperty = BarcodeReaderProperty(dict: _barcodeReaderPropertyDic)
    
    switch barcodeReaderProperty.barcodeReaderSdk {
    case .OPN3200Si:
      barcodeReaderProtocol = BarcodeReaderOPN3200Si(property: barcodeReaderProperty)
      barcodeReaderProtocol?.setup()
    default: break
    }
  }

  @objc(sendData:data:)
  func sendData(eventName: String,data: String) {
    self.sendEvent( withName: eventName, body: data)
  }
}
