#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(RNOrangeOperationBarcodeReader, NSObject)

RCT_EXTERN_METHOD(setBarcodeReaderProperty:(NSDictionary *)barcodeReaderPropertyDic)
RCT_EXTERN_METHOD(sendData: (NSString *)eventName data(NSData *)data)

@end
