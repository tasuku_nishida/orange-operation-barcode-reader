//
//  OPNSettingChar.h
//  OPNBluetoothKit
//
//  Copyright (c) 2012年 OPTOELECTRONICS CO., LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

//**************************************************************************************************
// @name OPN2002iBluetoothService
//**************************************************************************************************
/**
 * 文字列設定コマンドクラス<br/>
 * 1. ビット配列pfsfをOPN2002iでのプリフィックス又は、サフィックスの配列に変換します。
 * 2. 設定文字strをOPN2002iでのコマンド配列に変換します。
 * 3. OPN2002iでの文字列設定コマンド一覧を配列として返します。
 */

@interface OPNSettingChar : NSObject

/** 設定文字
 */
@property(strong,nonatomic)NSString *name;
/** コマンド
 */
@property(strong,nonatomic)NSString *command;
/** XZ5でのPFSF出力
 */
@property(nonatomic)int pfsf;

/**
 * ビット配列pfsfをOPN2002iでのプリフィックス又は、サフィックスの配列に変換します。
 * @param pfsf XZ5でのPFSF出力
 * @return プリフィックス又は、サフィックスの配列
 */
+ (NSMutableArray *)settingcharsWithPFSFs:(NSData *)pfsf;

/**
 * 設定文字strをOPN2002iでのコマンド配列に変換します。
 * @param str 変換対象文字列
 * @return コマンド配列
 */
+ (NSString *)getCommandString:(NSString *)str;

/**
 * OPN2002iでの文字列設定コマンド一覧を配列として返します。
 * @return 文字列設定コマンド一覧
 */
+ (NSArray *)createSettingChars;
@end