//
//  EADSessionController.h
//  OPNTerm
//
//  Copyright (c) 2012年 OPTOELECTRONICS CO., LTD. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <ExternalAccessory/ExternalAccessory.h>
#import "OPNSettings.h"

@class OPNBluetoothService;

//**************************************************************************************************
// @name OPNSessionControllerDelegate <NSObject>
//**************************************************************************************************


/** OPNSessionControllerのデリゲートです。
 */
@protocol OPNBluetoothServiceDelegate <NSObject>
@optional 
/** 正しく通信出来なかったことを通知します。
 * @param service OPNBluetoothServiceのインスタンスです。
 * @param error ERROR内容を返します。
 */
- (void)bluetoothService:(OPNBluetoothService *)service withError:(NSError *)error;
@end


//**************************************************************************************************
// @name OPNSessionController
//**************************************************************************************************


/** OPN2002iとのI/O処理、セッション管理、アクセサリ保持を担当するコアクラスです。
 * このクラスはアプリに1つのみ存在するSingletonクラスです。
 */
@interface OPNBluetoothService : NSObject <EAAccessoryDelegate, NSStreamDelegate>

/** 指定したEAAccesoryとprotocolStringを初期化します。
 * @param accessory アクセサリを指定します。
 * @param protocolString データコレクターのプロトコルストリングを指定します。
 */
- (void)setupControllerForAccessory:(EAAccessory *)accessory withProtocolString:(NSString *)protocolString;

/** EASessionを生成し、NSInputStreamとNSOutputStreamをオープンします。
 * 
 * 1. 現在保持しているEAAccessoryに自分自身をデリゲートとして設定します。
 * 2. EASessionを生成し取得します。生成時には、setupControllerForAccessory:accessory:withProtocolString 
 *    で指定したEAAccessoryとprotocolStringを使います。
 * 
 * @return BOOL EASessionを生成できた場合、YESを返します。それ以外はNOを返します。
 */
- (BOOL)openSession;

/** EASessionをクローズし、RunLoopを削除します。EASessionのリリースし、nilで初期化します。
 */
- (void)closeSession;



/** 指定したNSDataをセッションにデータを書き込みます。
 * @param data 追記するデータ
 */
- (void)writeData:(NSData *)data;



/** ACKを送信します。
 */
- (void)writeACK;

/** NAKを送信します。
 */
- (void)writeNAK;

/** 読み込んだデータのバイト長を返します。
 */
- (NSUInteger)readBytesAvailable;

/** bytesToReadで指定したバイト分を返します。
 * 読み込んだ部分はNULLで初期化されます。
 * @param bytesToRead 取得するバイナリデータの長さを指定します。
 * @return _readDataのうちbytesToReadで指定したバイト長のデータ
 */
- (NSData *)readData:(NSUInteger)bytesToRead;


/** 接続しているEAAccessory
 */
@property (weak, nonatomic, readonly) EAAccessory *accessory;

/** 接続しているデバイスのprotocolString
 */
@property (nonatomic, readonly) NSString *protocolString;

/** データ受信後に実行されるであろうデリゲート
 */
@property (nonatomic, weak) id<OPNBluetoothServiceDelegate> delegate;

/** 終端コード
 */
@property (nonatomic, strong) NSData *endCode;

@end
