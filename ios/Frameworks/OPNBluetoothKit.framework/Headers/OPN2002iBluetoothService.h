//
//  OPN2002iBluetoothService.h
//  OPNBluetoothKit
//
//  Copyright (c) 2012年 OPTOELECTRONICS CO., LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <OPNBluetoothKit/OPNBluetoothService.h>
#import <OPNBluetoothKit/OPNCommand.h>
#import <OPNBluetoothKit/OPNSettings.h>
#import <OPNBluetoothKit/OPNSettingChar.h>
#import <OPNBluetoothKit/OPNSettingFormat.h>


typedef enum{
    OPN2002I_READBORCODE_ENABLE,
    OPN2002I_READBORCODE_DISABLE
}OPN2002I_READBORCODE_PERMISSION;

@class OPN2002iBluetoothService;

//**************************************************************************************************
// @name OPN2002iBluetoothServiceDelegate
//**************************************************************************************************
/** OPN2002iBluetoothServiceのデリゲートです。
 */
@protocol OPN2002iBluetoothServiceDelegate <OPNBluetoothServiceDelegate>
@optional

/**
 *  GetFirmwareの結果を受信したことを通知します。
 *  @param service OPN2002iBluetoothServiceのインスタンスです。
 *  @param firmware GetFirmwareの受信結果です。
 */
- (void)bluetoothService:(OPN2002iBluetoothService *)service didGetFirmware:(NSData *)firmware;

/** ReadBarcodeの結果を受信したことを通知します。
 *  @param service OPN2002iBluetoothServiceのインスタンスです。
 *  @param data バーコードのReadBarcodeの受信結果です。
 */
- (void)bluetoothService:(OPN2002iBluetoothService *)service didReadBarcode:(NSData *)data;

/** GetStoredBarcodeAllの結果を受信したことを通知します。
 *  @param service OPN2002iBluetoothServiceのインスタンスです。
 *  @param data GetStoredBarcodeAllの受信結果です。
 */
- (void)bluetoothService:(OPN2002iBluetoothService *)service didGetStoredBarcodeAll:(NSData *)data;

/** SaveSettingの結果を受信したことを通知します。
 *  @param service OPN2002iBluetoothServiceのインスタンスです。
 *  @param data SaveSettingの受信結果です。
 */
- (void)bluetoothService:(OPN2002iBluetoothService *)service didSaveSetting:(NSData *)data;

/** SetDateAndTimeの結果を受信したことを通知します。
 *  @param service OPN2002iBluetoothServiceのインスタンスです。
 *  @param data SetDateAndTimeの受信結果です。
 */
- (void)bluetoothService:(OPN2002iBluetoothService *)service didSetDateAndTime:(NSData *)data;

/** GetDateAndTimeの結果を受信したことを通知します。
 *  @param service OPN2002iBluetoothServiceのインスタンスです。
 *  @param dateAndTime GetDateAndTimeの受信結果です。
 */
- (void)bluetoothService:(OPN2002iBluetoothService *)service didGetDateAndTime:(NSData *)dateAndTime;

/** GetBatteryLevelの結果を受信したことを通知します。
 *  @param service OPN2002iBluetoothServiceのインスタンスです。
 *  @param batteryLevel GetBatteryLevelの受信結果です。
 */
- (void)bluetoothService:(OPN2002iBluetoothService *)service didGetBatteryLevel:(NSData *)batteryLevel;

/** GetSettingの結果を受信したことを通知します。
 *  @param service OPN2002iBluetoothServiceのインスタンスです。
 *  @param setting GetSettingの受信結果です。
 */
- (void)bluetoothService:(OPN2002iBluetoothService *)service didGetSetting:(NSData *)setting;

/** GetBarcodeの結果を受信したことを通知します。
 *  @param service OPN2002iBluetoothServiceのインスタンスです。
 *  @param data GetBarcodeの受信結果です。
 */
- (void)bluetoothService:(OPN2002iBluetoothService *)service didGetBarcode:(NSData *)data;

/* Ackを受信したことを通知します。
 *  @param service OPN2002iBluetoothServiceのインスタンスです。
 *  @param data Ackの受信結果です。
 */
//- (void)bluetoothService:(OPN2002iBluetoothService *)service didGetAck:(NSData *)data;

/** InitializeSettingの結果を受信したことを通知します。
 *  @param service OPN2002iBluetoothServiceのインスタンスです。
 *  @param data InitializeSettingの受信結果です。
 */
- (void)bluetoothService:(OPN2002iBluetoothService *)service didInitializeSetting:(NSData *)data;

/** InitializeSettingBluetoothの結果を受信したことを通知します。
 *  @param service OPN2002iBluetoothServiceのインスタンスです。
 *  @param data InitializeSettingBluetoothの受信結果です。
 */
- (void)bluetoothService:(OPN2002iBluetoothService *)service didInitializeSettingBluetooth:(NSData *)data;

/** EraseStoredBarcodeAllの結果を受信したことを通知します。
 *  @param service OPN2002iBluetoothServiceのインスタンスです。
 *  @param data EraseStoredBarcodeAllの受信結果です。
 */
- (void)bluetoothService:(OPN2002iBluetoothService *)service didEraseStoredBarcodeAll:(NSData *)data;

/** VibratorOnの結果を受信したことを通知します。
 *  @param service OPN2002iBluetoothServiceのインスタンスです。
 *  @param data バーコードのVibratorOnの受信結果です。
 */
- (void)bluetoothService:(OPN2002iBluetoothService *)service didVibratorOn:(NSData *)data;

/** BeepBuzzerの結果を受信したことを通知します。
 *  @param service OPN2002iBluetoothServiceのインスタンスです。
 *  @param data バーコードのBeepBuzzerの受信結果です。
 */
- (void)bluetoothService:(OPN2002iBluetoothService *)service didBeepBuzzer:(NSData *)data;

/**
 *  全てのコマンド送信が終了したことを通知します。
 *  @param service OPN2002iBluetoothServiceのインスタンスです。
 */
- (void)bluetoothServiceCompletedCmd:(OPN2002iBluetoothService *)service;


@end


//**************************************************************************************************
// @name OPN2002iBluetoothService
//**************************************************************************************************
/**
 * データコレクターOPN2002iとSPP接続を行うためのOPN2002iBluetoothServiceクラス。<br/>
 * データコレクターに対する基本的なコマンドをメソッドとして提供しています。<br/>
 * [GoF]Singletonパターンを使って実装されているため、インスタンスは1つだけ生成されます。<br/>
 * インスタンスの取得には、OPN2002iBluetoothService#sharedController()を使用してください。<br/>
 */
@interface OPN2002iBluetoothService : OPNBluetoothService

/** OPN2002iBluetoothServiceのインスタンスを返します。
 * @return OPN2002iBluetoothServiceのインスタンスを返します。
 */
+ (OPN2002iBluetoothService *)sharedController;

/**
 * データコレクターにバイナリデータを送信します。
 * @param　data 送信する文字列
 * @param　delegate このクラスには、通信が正しく行われたことが通知されます。
 */
- (void)writeData:(NSData *)data target:(id<OPN2002iBluetoothServiceDelegate>)delegate;

/**
 * データコレクターにバイナリデータを送信します。
 * @param　data 送信する文字列
 * @param　delegate このクラスには、通信が正しく行われたことが通知されます。
 * @param　time タイムアウト時間(秒)
 */
- (void)writeData:(NSData *)data
           target:(id<OPN2002iBluetoothServiceDelegate>)delegate
          timeout:(NSTimeInterval)time;
/**
 * データコレクターにバイナリデータを送信します。
 * 通信時にReadBarcodeを受信した場合、Nakを返します。
 * @param　data 送信する文字列
 * @param　time タイムアウト時間(秒)
 * @param　delegate このクラスには、通信が正しく行われたことが通知されます。
 */
- (void)writeDataReadBarcodeResponseNak:(NSData *)data
                                timeout:(NSTimeInterval)time
                                 target:(id<OPN2002iBluetoothServiceDelegate>)delegate;

/**
 * データコレクターの全設定を初期化します。
 * @param　delegate このクラスには、通信が正しく行われたことが通知されます。
 */
- (void)initializeSetting:(id<OPN2002iBluetoothServiceDelegate>)delegate;


/**
 * データコレクターのBluetooth設定を初期化します。
 * @param　delegate このクラスには、通信が正しく行われたことが通知されます。
 */
- (void)initializeSettingBluetooth:(id<OPN2002iBluetoothServiceDelegate>)delegate;

/**
 * データコレクターのバージョンを取得します。
 * @param　delegate このクラスには、通信が正しく行われたことが通知されます。
 */
- (void)getFirmwareVersion:(id<OPN2002iBluetoothServiceDelegate>)delegate;

/**
 * データコレクターの揮発メモリに保存されている設定を、不揮発メモリに保存します。
 * @param　delegate このクラスには、通信が正しく行われたことが通知されます。
 */
- (void)saveSetting:(id<OPN2002iBluetoothServiceDelegate>)delegate;

/**
 * データコレクターでバーコード読取を開始します。
 * @param　delegate このクラスには、通信が正しく行われたことが通知されます。
 */
- (void)readBarcode:(id<OPN2002iBluetoothServiceDelegate>)delegate;

/**
 * データコレクターのメモリデータを出力します。
 * @param　delegate このクラスには、通信が正しく行われたことが通知されます。
 */
- (void)getStoredBarcodeAll:(id<OPN2002iBluetoothServiceDelegate>)delegate;

/**
 * データコレクターのメモリデータをクリアします。
 * @param　delegate このクラスには、通信が正しく行われたことが通知されます。
 */
- (void)eraseStoredBarcodeAll:(id<OPN2002iBluetoothServiceDelegate>)delegate;

/**
 * データコレクターの日時を設定します。
 * @param date 新たに設定する日付。
 * @param delegate このクラスには、通信が正しく行われたことが通知されます。
 */
- (void)setDateAndTime:(id<OPN2002iBluetoothServiceDelegate>)delegate date:(NSDate *)date;

/**
 * データコレクターに設定されている日時を取得します。
 * @param delegate このクラスには、通信が正しく行われたことが通知されます。
 */
- (void)getDateAndTime:(id<OPN2002iBluetoothServiceDelegate>)delegate;

/**
 * データコレクターの電池電圧を取得します。
 * @param delegate このクラスには、通信が正しく行われたことが通知されます。
 */
- (void)getBatteryLevel:(id<OPN2002iBluetoothServiceDelegate>)delegate;

/**
 * データコレクターの全設定を取得します。
 * @param delegate このクラスには、通信が正しく行われたことが通知されます。
 */
- (void)getSetting:(id<OPN2002iBluetoothServiceDelegate>)delegate;

/**
 * データコレクターのバイブレーターを振動させます。
 * @param　delegate このクラスには、通信が正しく行われたことが通知されます。
 */
- (void)vibratorOn:(id<OPN2002iBluetoothServiceDelegate>)delegate;

/**
 * データコレクターのブザーを鳴動させます。
 * @param　delegate このクラスには、通信が正しく行われたことが通知されます。
 */
- (void)beepBuzzer:(id<OPN2002iBluetoothServiceDelegate>)delegate;


/**
 * 未送信キューのコマンドを全てクリアします。
 */
- (void)cancelAllCommand;

/**
 * 未送信キューのコマンドを全て取得します。
 */
- (NSArray *)getCommandQueque;


/**
 * ReadBarcodeの許可/禁止のステータスを設定します。
 * @param status OPN2002I_READBORCODE_ENABLE:読み込み許可
 * OPN2002I_READBORCODE_DISABLE:読み込み禁止
 */
- (void)setEnableReadBarcode:(OPN2002I_READBORCODE_PERMISSION)status;

@end
