//
//  OPNCommand.h
//  OPNBluetoothKit
//
//  Copyright (c) 2012年 OPTOELECTRONICS CO., LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

/** 初期化コマンド */
extern NSString *const OPNInitializeSettingAllCommand;
/** Bluetooth初期化コマンド */
extern NSString *const OPNInitializeSettingBluetoothCommand;
/** バージョン表示コマンド */
extern NSString *const OPNGetFirmWareVersionCommand;
/** 設定保存コマンド */
extern NSString *const OPNSaveSettingCommand;
/** 読取開始コマンド */
extern NSString *const OPNReadBarcodeCommand;
/** メモリデータ出力コマンド */
extern NSString *const OPNGetStoredBarcodeAllCommand;
/** メモリデータクリアコマンド */
extern NSString *const OPNEraseStoredBarcodeAllCommand;
/** RTCの設定開始コマンド */
extern NSString *const OPNSetDateAndTimeStartCommand;
/**　RTCの設定終了コマンド */
extern NSString *const OPNSetDateAndTimeEndCommand;
/**　時刻取得コマンド */
extern NSString *const OPNGetDateAndTimeCommand;
/**　電池の電圧取得コマンド */
extern NSString *const OPNGetBatteryLevelCommand;
/**　現在の設定取得コマンド */
extern NSString *const OPNGetSettingCommand;
/**　PIN設定開始コマンド　*/
extern NSString *const OPNSetPinStartCommand;
/**　PIN設定終了コマンド　*/
extern NSString *const OPNSetPinEndCommand;
/**　接続相手アドレス設定開始コマンド　*/
extern NSString *const OPNSetRemoteDeviceAddressStartCommand;
/**　接続相手アドレス設定終了コマンド　*/
extern NSString *const OPNSetRemoteDeviceAddressEndCommand;
/**　デバイス名設定開始コマンド　*/
extern NSString *const OPNSetDeviceNameStartCommand;
/**　デバイス名設定終了コマンド　*/
extern NSString *const OPNSetDeviceNameEndCommand;
/** 設定開始/終了コマンド */
extern NSString *const OPNSettingSignalCommand;
/** 読取許可設定開始/終了コマンド */
extern NSString *const OPNSettingPermissionCommand;
/** ACK/NAK制御有効化コマンド */
extern NSString *const OPNCommandEnableAckNakControlCommand;
/** コマンド応答設定コマンド */
extern NSString *const OPNResponseCommandCommand;
/** バイブレータ振動コマンド */
extern NSString *const OPNVibratorOnCommand;
/** ブザー鳴動コマンド */
extern NSString *const OPNBeepBuzzerCommand;


//**************************************************************************************************
// @name NSString (OPNCommand)
//**************************************************************************************************

/** NSStringにOPNデバイスに送信するためのデータに変換する機能を追加します。
 */
@interface NSString(OPNCommand)

/** OPNデバイスに送信するためのデータに変換します。
 */
- (NSData *)dataCommand;

@end


