
import { NativeModules, Platform, NativeEventEmitter } from 'react-native';

const { RNOrangeOperationBarcodeReader } = NativeModules;

export default RNOrangeOperationBarcodeReader;

export function registerBarcodeReaderEvent (callback) {
  const eventEmitter = new NativeEventEmitter(NativeModules.RNOrangeOperationBarcodeReader)
  const event = eventEmitter.addListener('barcode', callback)
  
  if (Platform.OS === 'ios') {
    NativeModules.RNOrangeOperationBarcodeReader.setBarcodeReaderProperty({
      BARCODE_READER_SDK: 0,
      BARCODE_READER_STD: 1,
      BARCODE_READER_IP: 'empty'
    })
  }
  return event
}