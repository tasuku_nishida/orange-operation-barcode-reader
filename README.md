
# react-native-orange-operation-barcode-reader

## Getting started

`$ npm install react-native-orange-operation-barcode-reader --save`

### Mostly automatic installation

`$ react-native link react-native-orange-operation-barcode-reader`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-orange-operation-barcode-reader` and add `RNOrangeOperationBarcodeReader.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNOrangeOperationBarcodeReader.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNOrangeOperationBarcodeReaderPackage;` to the imports at the top of the file
  - Add `new RNOrangeOperationBarcodeReaderPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-orange-operation-barcode-reader'
  	project(':react-native-orange-operation-barcode-reader').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-orange-operation-barcode-reader/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-orange-operation-barcode-reader')
  	```

#### Windows
[Read it! :D](https://github.com/ReactWindows/react-native)

1. In Visual Studio add the `RNOrangeOperationBarcodeReader.sln` in `node_modules/react-native-orange-operation-barcode-reader/windows/RNOrangeOperationBarcodeReader.sln` folder to their solution, reference from their app.
2. Open up your `MainPage.cs` app
  - Add `using Orange.Operation.Barcode.Reader.RNOrangeOperationBarcodeReader;` to the usings at the top of the file
  - Add `new RNOrangeOperationBarcodeReaderPackage()` to the `List<IReactPackage>` returned by the `Packages` method


## Usage
```javascript
import RNOrangeOperationBarcodeReader from 'react-native-orange-operation-barcode-reader';

// TODO: What to do with the module?
RNOrangeOperationBarcodeReader;
```
  