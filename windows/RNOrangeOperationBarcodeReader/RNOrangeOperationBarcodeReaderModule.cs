using ReactNative.Bridge;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace Orange.Operation.Barcode.Reader.RNOrangeOperationBarcodeReader
{
    /// <summary>
    /// A module that allows JS to share data.
    /// </summary>
    class RNOrangeOperationBarcodeReaderModule : NativeModuleBase
    {
        /// <summary>
        /// Instantiates the <see cref="RNOrangeOperationBarcodeReaderModule"/>.
        /// </summary>
        internal RNOrangeOperationBarcodeReaderModule()
        {

        }

        /// <summary>
        /// The name of the native module.
        /// </summary>
        public override string Name
        {
            get
            {
                return "RNOrangeOperationBarcodeReader";
            }
        }
    }
}
